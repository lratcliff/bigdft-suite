'''This is the configuration file for the BigDFT installer
for use on the Fugaku computer when cross compiling.

1. Source SPACK
$ . /vol0001/apps/oss/spack/share/spack/setup-env.sh

2. Load in cmake and pip
spack load cmake@3.21.4%gcc@11.2.0
spack load py-pip@21.1.2%gcc@11.2.0
Use pip to install six and future

3. Now you are ready to compile.
'''

conditions.add("testing")

# List the module the this rcfile will build
moduleset = 'bigdft'
modules = ['spred', ]


def env_configuration():
    '''
    Species the configure line for various packages.
    '''
    from os import getcwd, path
    env = {}
    env["FC"] = "mpifrtpx"
    env["F77"] = "frtpx"
    env["CC"] = "fccpx"
    env["CXX"] = "FCCpx"
    env["FCFLAGS"] = "-SSL2BLAMP -Kfast,openmp,noautoobjstack"
    env["FFLAGS"] = "-Kfast"
    env["CFLAGS"] = "-g -std=gnu99 -O2 -I" + path.join(getcwd(), "install", "include")
    env["--with-ext-linalg"] = "-fjlapackex -SCALAPACK"
    env["LIBS"] = "-SSL2BLAMP -Kfast,openmp -Nlibomp"
    env["--build"] = "x86_64-redhat-linux"
    env["--host"] = "sparc-fujitsu-linux"

    return " ".join(['"' + x + '=' + y + '"' for x, y in env.items()])

def ntpoly_configuration():
    ''' 
    For NTPoly we need to specify the cmake options.
    ''' 
    from os import getcwd, path

    cmake_flags = {}
    cmake_flags["CMAKE_Fortran_FLAGS_RELEASE"] = "-SSL2BLAMP -Kfast,openmp"
    cmake_flags["CMAKE_Fortran_COMPILER"] = "mpifrtpx"
    cmake_flags["CMAKE_C_COMPILER"] = "mpifccpx"
    cmake_flags["CMAKE_CXX_COMPILER"] = "mpiFCCpx"
    cmake_flags["CMAKE_PREFIX_PATH"] = path.join(getcwd(), "install")
    cmake_flags["CMAKE_Fortran_MODDIR_FLAG"] = "-M"

    return " ".join(['-D' + x + '="' + y + '"' for x, y in cmake_flags.items()])

autogenargs = env_configuration()
module_cmakeargs.update({
    'ntpoly': ntpoly_configuration()
})

