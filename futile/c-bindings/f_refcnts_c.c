#include "f_refcnts.h"
#include <config.h>
#include <string.h>

void FC_FUNC_(bind_f90_f_reference_counter_copy_constructor, BIND_F90_F_REFERENCE_COUNTER_COPY_CONSTRUCTOR)(f90_f_reference_counter_pointer*,
  const f90_f_reference_counter*);
f90_f_reference_counter_pointer f90_f_reference_counter_copy_constructor(const f90_f_reference_counter* other)
{
  f90_f_reference_counter_pointer out_self;
  FC_FUNC_(bind_f90_f_reference_counter_copy_constructor, BIND_F90_F_REFERENCE_COUNTER_COPY_CONSTRUCTOR)
    (&out_self, other);
  return out_self;
}

void FC_FUNC_(bind_f_associated, BIND_F_ASSOCIATED)(int*,
  const f90_f_reference_counter*);
bool f_associated(const f90_f_reference_counter* ref)
{
  int out_f_associated;
  FC_FUNC_(bind_f_associated, BIND_F_ASSOCIATED)
    (&out_f_associated, ref);
  return out_f_associated;
}

void FC_FUNC_(bind_f_ref_null, BIND_F_REF_NULL)(f90_f_reference_counter_pointer*);
f90_f_reference_counter_pointer f_ref_null(void)
{
  f90_f_reference_counter_pointer out_ref;
  FC_FUNC_(bind_f_ref_null, BIND_F_REF_NULL)
    (&out_ref);
  return out_ref;
}

void FC_FUNC_(bind_f_ref_new, BIND_F_REF_NEW)(f90_f_reference_counter_pointer*,
  const char*,
  const size_t*,
  const void**,
  size_t);
f90_f_reference_counter_pointer f_ref_new(const char* id,
  const void* (*address))
{
  f90_f_reference_counter_pointer out_ref;
  size_t id_chk_len, id_len = id_chk_len = id ? strlen(id) : 0;
  FC_FUNC_(bind_f_ref_new, BIND_F_REF_NEW)
    (&out_ref, id, &id_len, address, id_chk_len);
  return out_ref;
}

void FC_FUNC_(bind_f_ref_count, BIND_F_REF_COUNT)(int*,
  const f90_f_reference_counter*);
int f_ref_count(const f90_f_reference_counter* ref)
{
  int out_count;
  FC_FUNC_(bind_f_ref_count, BIND_F_REF_COUNT)
    (&out_count, ref);
  return out_count;
}

void FC_FUNC_(bind_refcnts_errors, BIND_REFCNTS_ERRORS)(void);
void refcnts_errors(void)
{
  FC_FUNC_(bind_refcnts_errors, BIND_REFCNTS_ERRORS)
    ();
}

void FC_FUNC_(bind_nullify_f_ref, BIND_NULLIFY_F_REF)(f90_f_reference_counter*);
void nullify_f_ref(f90_f_reference_counter* ref)
{
  FC_FUNC_(bind_nullify_f_ref, BIND_NULLIFY_F_REF)
    (ref);
}

void FC_FUNC_(bind_f_unref, BIND_F_UNREF)(f90_f_reference_counter*,
  int*);
void f_unref(f90_f_reference_counter* ref,
  int (*count))
{
  FC_FUNC_(bind_f_unref, BIND_F_UNREF)
    (ref, count);
}

void FC_FUNC_(bind_f_ref_free, BIND_F_REF_FREE)(f90_f_reference_counter*);
void f_ref_free(f90_f_reference_counter* ref)
{
  FC_FUNC_(bind_f_ref_free, BIND_F_REF_FREE)
    (ref);
}

void FC_FUNC_(bind_f_ref, BIND_F_REF)(f90_f_reference_counter*);
void f_ref(f90_f_reference_counter* src)
{
  FC_FUNC_(bind_f_ref, BIND_F_REF)
    (src);
}

void FC_FUNC_(bind_f_ref_associate, BIND_F_REF_ASSOCIATE)(const f90_f_reference_counter*,
  f90_f_reference_counter*);
void f_ref_associate(const f90_f_reference_counter* src,
  f90_f_reference_counter* dest)
{
  FC_FUNC_(bind_f_ref_associate, BIND_F_REF_ASSOCIATE)
    (src, dest);
}

